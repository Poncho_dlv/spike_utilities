#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List

from fuzzywuzzy import process, fuzz

from spike_model.Competition import Competition
from spike_requester.CyanideApi import CyanideApi


class FuzzyUtils:

    async def get_best_match_competition(self, league_list: List, searched_competition: str, platform: str):
        best_match_competition = None
        competition_data = None
        for league in league_list:
            data = CyanideApi.get_competitions(league, platform=platform)
            if data is not None:
                competitions_list = []
                for competition in data["competitions"]:
                    if competition["status"] is not None and competition["status"] < 2:
                        competitions_list.append(competition["name"])

                if len(competitions_list) > 0:
                    ret = self.get_best_match_into_list(searched_competition, competitions_list)
                    if ret is not None:
                        if best_match_competition is None or ret[1] > best_match_competition[1]:
                            best_match_competition = ret
                            competition_data = data["competitions"]

        if best_match_competition is not None:
            for competition in competition_data:
                competition = Competition(competition)
                if competition.get_name() == best_match_competition[0]:
                    return competition
        else:
            return None

    @staticmethod
    def get_best_match_into_list(key: str, values: List[str]):
        ret = process.extractOne(key, values, scorer=fuzz.ratio, score_cutoff=55)
        if ret is None:
            ret = process.extractOne(key, values, scorer=fuzz.partial_ratio, score_cutoff=75)

        return ret
