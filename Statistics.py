#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import logging

from spike_database.Alias import Alias, AliasType
from spike_database.BB2Resources import BB2Resources
from spike_database.Coaches import Coaches
from spike_database.CoachesStatistics import CoachesStatistics, StatisticType
from spike_database.Contests import Contests
from spike_database.MatchHistory import MatchHistory
from spike_database.Matchup import Matchup
from spike_database.TopCoaches import TopCoaches
from spike_model.Contest import Contest
from spike_requester.SpikeAPI.CclAPI import CclAPI
from spike_requester.SpikeAPI.StatisticsAPI import StatisticsAPI

spike_logger = logging.getLogger("spike_logger")

STATISTICS_VERSION = 3
LOW_TV_THRESHOLD = 1100
MID_TV_THRESHOLD = 1400
HIGH_TV_THRESHOLD = 1750

COMMON_STAT_TEMPLATE = {
    "win": 0,
    "draw": 0,
    "loss": 0,
    "win_rate": 0,
    "games_played": 0,
    "duration": 0,
    "mean_duration": 0,
    "concession": 0,
    "concession_rate": 0,
    "td_for": 0,
    "td_against": 0,
    "td_diff": 0,
    "min_tv": 50000,
    "max_tv": 0
}

COMMON_STAT_TEMPLATE_RACES = copy.deepcopy(COMMON_STAT_TEMPLATE)
COMMON_STAT_TEMPLATE_RACES["races_for"] = {}
COMMON_STAT_TEMPLATE_RACES["races_against"] = {}

TV_RANGE_TEMPLATE = {
    "all_tv": {"overall": copy.deepcopy(COMMON_STAT_TEMPLATE_RACES), "monthly": {}},
    "low_tv": {"overall": copy.deepcopy(COMMON_STAT_TEMPLATE_RACES), "monthly": {}},
    "mid_tv": {"overall": copy.deepcopy(COMMON_STAT_TEMPLATE_RACES), "monthly": {}},
    "high_tv": {"overall": copy.deepcopy(COMMON_STAT_TEMPLATE_RACES), "monthly": {}},
    "very_high_tv": {"overall": copy.deepcopy(COMMON_STAT_TEMPLATE_RACES), "monthly": {}}
}
TV_RANGE = TV_RANGE_TEMPLATE.keys()


class Statistics:

    @staticmethod
    def compute_and_save_top_league(league_name: str, platform_id: int):
        coach_db = Coaches()
        top_coach_db = TopCoaches()
        coaches = coach_db.get_leagues_top_coach(league_name, platform_id)
        overall_top_coach_rank = {}
        race_top_coach_rank = {}

        for tv_range in TV_RANGE:
            for coach in coaches:
                coach_league_stat = next(
                    (league for league in coach["statistics"]["leagues"] if league[tv_range]["overall"].get("name") == league_name), None)
                if coach_league_stat is not None:
                    # Overall stat
                    overall_data = {
                        "coach_name": coach.get("name"),
                        "coach_id": coach.get("id")
                    }
                    overall_data.update(coach_league_stat[tv_range]["overall"])
                    if overall_top_coach_rank.get(tv_range) is None:
                        overall_top_coach_rank[tv_range] = []
                    overall_top_coach_rank[tv_range].append(overall_data)

                    # Race stats
                    for race in coach_league_stat[tv_range]["overall"]["races_for"]:
                        race_name = race["name"]
                        if race_top_coach_rank.get(tv_range) is None:
                            overall_top_coach_rank[tv_range] = {}
                        if race_top_coach_rank.get(race_name) is None:
                            race_top_coach_rank[race_name] = []

                        race_data = {
                            "coach_name": coach.get("name"),
                            "coach_id": coach.get("id")
                        }
                        race_data.update(race)
                        race_top_coach_rank[race_name].append(race_data)
        top_coach_db.save_league_top_coaches(league_name, platform_id, overall_top_coach_rank, race_top_coach_rank)

    @staticmethod
    def update_root_stats(entry: dict, match: dict):
        entry["games_played"] += 1
        entry["duration"] += match["duration"]

    @staticmethod
    def update_team_stats(entry: dict, match: dict, team_label: str, opponent_label: str, result_label: str, race_for: str, race_against: str):
        entry[result_label] += 1
        entry["concession"] += int(match[team_label].get("conceded", 0))
        entry["td_for"] += int(match[team_label]["score"])
        entry["td_against"] += int(match[opponent_label]["score"])
        entry["min_tv"] = min(entry["min_tv"], int(match[team_label]["tv"]))
        entry["max_tv"] = max(entry["max_tv"], int(match[team_label]["tv"]))

        if entry.get("races_for", {}).get(race_for) is not None:
            entry["races_for"][race_for][result_label] += 1
            entry["races_for"][race_for]["concession"] += int(match[team_label].get("conceded", 0))
            entry["races_for"][race_for]["td_for"] += int(match[team_label]["score"])
            entry["races_for"][race_for]["td_against"] += int(match[opponent_label]["score"])
            entry["races_for"][race_for]["min_tv"] = min(entry["races_for"][race_for]["min_tv"], int(match[team_label]["tv"]))
            entry["races_for"][race_for]["max_tv"] = max(entry["races_for"][race_for]["max_tv"], int(match[team_label]["tv"]))

        if entry.get("races_against", {}).get(race_against) is not None:
            entry["races_against"][race_against][result_label] += 1
            entry["races_against"][race_against]["concession"] += int(match[team_label].get("conceded", 0))
            entry["races_against"][race_against]["td_for"] += int(match[team_label]["score"])
            entry["races_against"][race_against]["td_against"] += int(match[opponent_label]["score"])
            entry["races_against"][race_against]["min_tv"] = min(entry["races_against"][race_against]["min_tv"], int(match[team_label]["tv"]))
            entry["races_against"][race_against]["max_tv"] = max(entry["races_against"][race_against]["max_tv"], int(match[team_label]["tv"]))

    @staticmethod
    def update_specific_stats(entry: dict, root_key: str, sub_key: str, month_label: str, tv_label: str, match: dict, race_for: str,
                              race_against: str, sub_key_id: int = None):
        # Init current entry
        if entry[root_key].get(sub_key) is None:
            entry[root_key][sub_key] = copy.deepcopy(TV_RANGE_TEMPLATE)
            for tv_range in TV_RANGE:
                entry[root_key][sub_key][tv_range]["overall"]["name"] = sub_key
            if sub_key_id is not None:
                for tv_range in TV_RANGE:
                    entry[root_key][sub_key][tv_range]["overall"]["id"] = sub_key_id

        for tv_range in ["all_tv", tv_label]:
            if entry[root_key][sub_key][tv_range]["monthly"].get(month_label) is None:
                entry[root_key][sub_key][tv_range]["monthly"][month_label] = copy.deepcopy(COMMON_STAT_TEMPLATE_RACES)
                entry[root_key][sub_key][tv_range]["monthly"][month_label]["label"] = month_label
            if entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_for"].get(race_for) is None:
                entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_for"][race_for] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_for"][race_for]["name"] = race_for
            if entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_against"].get(race_against) is None:
                entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_against"][race_against] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_against"][race_against]["name"] = race_for

            if entry[root_key][sub_key][tv_range]["overall"]["races_for"].get(race_for) is None:
                entry[root_key][sub_key][tv_range]["overall"]["races_for"][race_for] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[root_key][sub_key][tv_range]["overall"]["races_for"][race_for]["name"] = race_for
            if entry[root_key][sub_key][tv_range]["overall"]["races_against"].get(race_against) is None:
                entry[root_key][sub_key][tv_range]["overall"]["races_against"][race_against] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[root_key][sub_key][tv_range]["overall"]["races_against"][race_against]["name"] = race_against

            Statistics.update_root_stats(entry[root_key][sub_key][tv_range]["overall"], match)
            Statistics.update_root_stats(entry[root_key][sub_key][tv_range]["overall"]["races_for"][race_for], match)
            Statistics.update_root_stats(entry[root_key][sub_key][tv_range]["overall"]["races_against"][race_against], match)
            Statistics.update_root_stats(entry[root_key][sub_key][tv_range]["monthly"][month_label], match)
            Statistics.update_root_stats(entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_for"][race_for], match)
            Statistics.update_root_stats(entry[root_key][sub_key][tv_range]["monthly"][month_label]["races_against"][race_against], match)

    @staticmethod
    def update_overall_stats(entry: dict, month_label: str, tv_label: str, match: dict, race_for: str, race_against: str):
        for tv_range in ["all_tv", tv_label]:
            if entry[tv_range]["monthly"].get(month_label) is None:
                entry[tv_range]["monthly"][month_label] = copy.deepcopy(COMMON_STAT_TEMPLATE_RACES)
                entry[tv_range]["monthly"][month_label]["label"] = month_label
            if entry[tv_range]["monthly"][month_label]["races_for"].get(race_for) is None:
                entry[tv_range]["monthly"][month_label]["races_for"][race_for] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[tv_range]["monthly"][month_label]["races_for"][race_for]["name"] = race_for
            if entry[tv_range]["monthly"][month_label]["races_against"].get(race_against) is None:
                entry[tv_range]["monthly"][month_label]["races_against"][race_against] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[tv_range]["monthly"][month_label]["races_against"][race_against]["name"] = race_for

            if entry[tv_range]["overall"]["races_for"].get(race_for) is None:
                entry[tv_range]["overall"]["races_for"][race_for] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[tv_range]["overall"]["races_for"][race_for]["name"] = race_for
            if entry[tv_range]["overall"]["races_against"].get(race_against) is None:
                entry[tv_range]["overall"]["races_against"][race_against] = copy.deepcopy(COMMON_STAT_TEMPLATE)
                entry[tv_range]["overall"]["races_against"][race_against]["name"] = race_against

            Statistics.update_root_stats(entry[tv_range]["overall"], match)
            Statistics.update_root_stats(entry[tv_range]["overall"]["races_for"][race_for], match)
            Statistics.update_root_stats(entry[tv_range]["overall"]["races_against"][race_against], match)
            Statistics.update_root_stats(entry[tv_range]["monthly"][month_label], match)
            Statistics.update_root_stats(entry[tv_range]["monthly"][month_label]["races_for"][race_for], match)
            Statistics.update_root_stats(entry[tv_range]["monthly"][month_label]["races_against"][race_against], match)

    @staticmethod
    def compute_statistics(entry: dict):
        # Lambda def
        def process_race1(races_list_data):
            for current_race in races_list_data:
                if current_race["games_played"] > 0:
                    current_race["win_rate"] = round(100 * (current_race["win"] + (current_race["draw"] / 2.)) / current_race["games_played"], 2)
                    current_race["mean_duration"] = round(current_race["duration"] / current_race["games_played"], 0)

                if current_race["concession"] > 0:
                    current_race["concession_rate"] = round(current_race["games_played"] / current_race["concession"], 0)
                current_race["td_diff"] = current_race["td_for"] - current_race["td_against"]

        # Lambda def
        def process_race2(races_list_data):
            for current_race in races_list_data:
                if current_race["games_played"] > 0:
                    current_race["win_rate"] = round(100 * (current_race["win"] + (current_race["draw"] / 2.)) / current_race["games_played"], 2)
                    current_race["mean_duration"] = round(current_race["duration"] / current_race["games_played"], 0)
                if current_race["concession"] > 0:
                    current_race["concession_rate"] = round(current_race["games_played"] / current_race["concession"], 0)
                current_race["td_diff"] = current_race["td_for"] - current_race["td_against"]
            races_list_data = list(races_list_data)
            races_list_data = sorted(races_list_data, key=lambda i: i["games_played"])
            return races_list_data

        for tv_range in TV_RANGE:
            if entry[tv_range]["overall"]["games_played"] > 0:
                entry[tv_range]["overall"]["win_rate"] = round(
                    100 * (entry[tv_range]["overall"]["win"] + (entry[tv_range]["overall"]["draw"] / 2.)) / entry[tv_range]["overall"][
                        "games_played"], 2)
                entry[tv_range]["overall"]["mean_duration"] = round(
                    entry[tv_range]["overall"]["duration"] / entry[tv_range]["overall"]["games_played"], 0)

            if entry[tv_range]["overall"]["concession"] > 0:
                entry[tv_range]["overall"]["concession_rate"] = round(
                    entry[tv_range]["overall"]["games_played"] / entry[tv_range]["overall"]["concession"], 0)
            entry[tv_range]["overall"]["td_diff"] = entry[tv_range]["overall"]["td_for"] - entry[tv_range]["overall"]["td_against"]

            process_race1(entry[tv_range]["overall"]["races_for"].values())
            process_race1(entry[tv_range]["overall"]["races_against"].values())

            for month in entry[tv_range]["monthly"].values():
                if month["games_played"] > 0:
                    win_pct = round(100 * (month["win"] + (month["draw"] / 2.)) / month["games_played"], 2)
                    month["win_rate"] = win_pct
                    month["mean_duration"] = round(month["duration"] / month["games_played"], 0)

                if month["concession"] > 0:
                    month["concession_rate"] = round(month["games_played"] / month["concession"], 0)
                month["td_diff"] = month["td_for"] - month["td_against"]

                if month.get("races_for") is not None:
                    month["races_for"] = process_race2(month["races_for"].values())
                if month.get("races_against") is not None:
                    month["races_against"] = process_race2(month["races_against"].values())

            entry[tv_range]["monthly"] = list(entry[tv_range]["monthly"].values())
            entry[tv_range]["monthly"] = sorted(entry[tv_range]["monthly"], key=lambda i: i["label"])

    @staticmethod
    def compute_and_save_coaches_win_rate(coach_id: int, platform_id: int = 1):
        match_history_db = MatchHistory()
        coach_stats_db = CoachesStatistics()
        bb_rsrc_db = BB2Resources()
        matches = match_history_db.get_match_history(coach_id, platform_id)
        alias_db = Alias()
        competitions_alias_list = alias_db.get_alias(AliasType.COMPETITION, platform_id)
        leagues_alias_list = alias_db.get_alias(AliasType.LEAGUE, platform_id)
        ccl_season = CclAPI.get_seasons(platform_id)
        if ccl_season is None or ccl_season.get('data') is None:
            spike_logger.error("ccl_season is None")
            spike_logger.error("platform_id: {}, coach_id: {}".format(platform_id, coach_id))
        statistics = {
            "version": STATISTICS_VERSION,
            "overall": copy.deepcopy(TV_RANGE_TEMPLATE),
            "leagues": {},
            "competitions": {},
            "ranked_ladder": copy.deepcopy(TV_RANGE_TEMPLATE),
            "ranked_playoffs": copy.deepcopy(TV_RANGE_TEMPLATE),
        }

        ranked_ladder = []
        ranked_playoffs = []
        ccl_season = ccl_season['data']
        if ccl_season is not None:
            for ladder in ccl_season.get("ladders", []):
                ranked_ladder.append(ladder.get("name"))
            for playoff in ccl_season.get("playoffs", []):
                ranked_playoffs.append(playoff.get("name"))

        for match in matches:
            if int(match.get("duration", 0)) > 0:  # Ignore pres game concession
                month_label = "{}-{}".format(match["date"].split("-")[0], match["date"].split("-")[1])
                league_name = Statistics.get_league_name_alias(match["league_name"], platform_id, leagues_alias_list)
                competition_name = Statistics.get_competition_name_alias(league_name, match["competition_name"], platform_id, competitions_alias_list)

                if match["team_home"]["coach_id"] == coach_id:
                    race_for = bb_rsrc_db.get_race_label(match["team_home"]["race_id"])
                    race_against = bb_rsrc_db.get_race_label(match["team_away"]["race_id"])
                    if int(match["team_home"]["tv"]) <= LOW_TV_THRESHOLD:
                        tv_label = "low_tv"
                    elif int(match["team_home"]["tv"]) <= MID_TV_THRESHOLD:
                        tv_label = "mid_tv"
                    elif int(match["team_home"]["tv"]) <= HIGH_TV_THRESHOLD:
                        tv_label = "high_tv"
                    else:
                        tv_label = "very_high_tv"
                    team_label = "team_home"
                    opponent_label = "team_away"
                else:
                    race_for = bb_rsrc_db.get_race_label(match["team_away"]["race_id"])
                    race_against = bb_rsrc_db.get_race_label(match["team_home"]["race_id"])
                    if int(match["team_away"]["tv"]) <= LOW_TV_THRESHOLD:
                        tv_label = "low_tv"
                    elif int(match["team_away"]["tv"]) <= MID_TV_THRESHOLD:
                        tv_label = "mid_tv"
                    elif int(match["team_away"]["tv"]) <= HIGH_TV_THRESHOLD:
                        tv_label = "high_tv"
                    else:
                        tv_label = "very_high_tv"
                    team_label = "team_away"
                    opponent_label = "team_home"

                # Update overall stat
                Statistics.update_overall_stats(statistics["overall"], month_label, tv_label, match, race_for, race_against)

                # Update leagues stats
                Statistics.update_specific_stats(statistics, "leagues", league_name, month_label, tv_label, match, race_for, race_against)

                # Update competitions stats
                Statistics.update_specific_stats(statistics, "competitions", competition_name, month_label, tv_label, match, race_for, race_against)

                if league_name == "Cabalvision Official League":
                    if competition_name in ranked_ladder:
                        # Update ranked ladder stats
                        Statistics.update_overall_stats(statistics["ranked_ladder"], month_label, tv_label, match, race_for, race_against)
                    elif competition_name in ranked_playoffs:
                        # Update ranked playoffs stats
                        Statistics.update_overall_stats(statistics["ranked_playoffs"], month_label, tv_label, match, race_for, race_against)

                if match["team_home"]["score"] == match["team_away"]["score"]:
                    for tv_range in ["all_tv", tv_label]:
                        # Update overall stat
                        Statistics.update_team_stats(statistics["overall"][tv_range]["overall"], match, team_label, opponent_label, "draw", race_for,
                                                     race_against)
                        Statistics.update_team_stats(statistics["overall"][tv_range]["monthly"][month_label], match, team_label, opponent_label,
                                                     "draw", race_for, race_against)
                        # Update leagues
                        Statistics.update_team_stats(statistics["leagues"][league_name][tv_range]["overall"], match, team_label, opponent_label,
                                                     "draw", race_for, race_against)
                        Statistics.update_team_stats(statistics["leagues"][league_name][tv_range]["monthly"][month_label], match, team_label,
                                                     opponent_label, "draw", race_for, race_against)
                        # Update competitions
                        Statistics.update_team_stats(statistics["competitions"][competition_name][tv_range]["overall"], match, team_label,
                                                     opponent_label, "draw", race_for, race_against)
                        Statistics.update_team_stats(statistics["competitions"][competition_name][tv_range]["monthly"][month_label], match,
                                                     team_label, opponent_label, "draw", race_for, race_against)
                        if league_name == "Cabalvision Official League":
                            if competition_name in ranked_ladder:
                                # Update ranked ladder stats
                                Statistics.update_team_stats(statistics["ranked_ladder"][tv_range]["overall"], match, team_label, opponent_label,
                                                             "draw", race_for, race_against)
                                Statistics.update_team_stats(statistics["ranked_ladder"][tv_range]["monthly"][month_label], match, team_label,
                                                             opponent_label, "draw", race_for, race_against)
                            elif competition_name in ranked_playoffs:
                                # Update ranked playoffs stats
                                Statistics.update_team_stats(statistics["ranked_playoffs"][tv_range]["overall"], match, team_label, opponent_label,
                                                             "draw", race_for, race_against)
                                Statistics.update_team_stats(statistics["ranked_playoffs"][tv_range]["monthly"][month_label], match, team_label,
                                                             opponent_label, "draw", race_for, race_against)

                elif match[team_label]["score"] > match[opponent_label]["score"]:
                    for tv_range in ["all_tv", tv_label]:
                        # Update overall stat
                        Statistics.update_team_stats(statistics["overall"][tv_range]["overall"], match, team_label, opponent_label, "win", race_for,
                                                     race_against)
                        Statistics.update_team_stats(statistics["overall"][tv_range]["monthly"][month_label], match, team_label, opponent_label,
                                                     "win", race_for, race_against)
                        # Update leagues
                        Statistics.update_team_stats(statistics["leagues"][league_name][tv_range]["overall"], match, team_label, opponent_label,
                                                     "win", race_for, race_against)
                        Statistics.update_team_stats(statistics["leagues"][league_name][tv_range]["monthly"][month_label], match, team_label,
                                                     opponent_label, "win", race_for, race_against)
                        # Update competitions
                        Statistics.update_team_stats(statistics["competitions"][competition_name][tv_range]["overall"], match, team_label,
                                                     opponent_label, "win", race_for, race_against)
                        Statistics.update_team_stats(statistics["competitions"][competition_name][tv_range]["monthly"][month_label], match,
                                                     team_label, opponent_label, "win", race_for, race_against)
                        if league_name == "Cabalvision Official League":
                            if competition_name in ranked_ladder:
                                # Update ranked ladder stats
                                Statistics.update_team_stats(statistics["ranked_ladder"][tv_range]["overall"], match, team_label, opponent_label,
                                                             "win", race_for, race_against)
                                Statistics.update_team_stats(statistics["ranked_ladder"][tv_range]["monthly"][month_label], match, team_label,
                                                             opponent_label, "win", race_for, race_against)
                            elif competition_name in ranked_playoffs:
                                # Update ranked playoffs stats
                                Statistics.update_team_stats(statistics["ranked_playoffs"][tv_range]["overall"], match, team_label, opponent_label,
                                                             "win", race_for, race_against)
                                Statistics.update_team_stats(statistics["ranked_playoffs"][tv_range]["monthly"][month_label], match, team_label,
                                                             opponent_label, "win", race_for, race_against)
                else:
                    for tv_range in ["all_tv", tv_label]:
                        # Update overall stat
                        Statistics.update_team_stats(statistics["overall"][tv_range]["overall"], match, team_label, opponent_label, "loss", race_for,
                                                     race_against)
                        Statistics.update_team_stats(statistics["overall"][tv_range]["monthly"][month_label], match, team_label, opponent_label,
                                                     "loss", race_for, race_against)
                        # Update leagues
                        Statistics.update_team_stats(statistics["leagues"][league_name][tv_range]["overall"], match, team_label, opponent_label,
                                                     "loss", race_for, race_against)
                        Statistics.update_team_stats(statistics["leagues"][league_name][tv_range]["monthly"][month_label], match, team_label,
                                                     opponent_label, "loss", race_for, race_against)
                        # Update competitions
                        Statistics.update_team_stats(statistics["competitions"][competition_name][tv_range]["overall"], match, team_label,
                                                     opponent_label, "loss", race_for, race_against)
                        Statistics.update_team_stats(statistics["competitions"][competition_name][tv_range]["monthly"][month_label], match,
                                                     team_label, opponent_label, "loss", race_for, race_against)
                        if league_name == "Cabalvision Official League":
                            if competition_name in ranked_ladder:
                                # Update ranked ladder stats
                                Statistics.update_team_stats(statistics["ranked_ladder"][tv_range]["overall"], match, team_label, opponent_label,
                                                             "loss", race_for, race_against)
                                Statistics.update_team_stats(statistics["ranked_ladder"][tv_range]["monthly"][month_label], match, team_label,
                                                             opponent_label, "loss", race_for, race_against)
                            elif competition_name in ranked_playoffs:
                                # Update ranked playoffs stats
                                Statistics.update_team_stats(statistics["ranked_playoffs"][tv_range]["overall"], match, team_label, opponent_label,
                                                             "loss", race_for, race_against)
                                Statistics.update_team_stats(statistics["ranked_playoffs"][tv_range]["monthly"][month_label], match, team_label,
                                                             opponent_label, "loss", race_for, race_against)

        Statistics.compute_statistics(statistics["overall"])
        for entry in statistics["leagues"].values():
            Statistics.compute_statistics(entry)
        for entry in statistics["competitions"].values():
            Statistics.compute_statistics(entry)
        Statistics.compute_statistics(statistics["ranked_ladder"])
        Statistics.compute_statistics(statistics["ranked_playoffs"])

        saved_stats = []

        def data_formatter(items, stat_type: StatisticType, entry_name: str = None):
            for key, value in items:
                stat_data = {
                    "coach_id": coach_id,
                    "platform_id": platform_id,
                    "tv_range": key,
                    "version": STATISTICS_VERSION,
                    "type": stat_type,
                    "overall": value.get("overall", {}),
                    "monthly": value.get("monthly", {})
                }
                if entry_name is not None:
                    stat_data["name"] = entry_name
                saved_stats.append(stat_data)

        data_formatter(statistics["overall"].items(), StatisticType.OVERALL)
        data_formatter(statistics["ranked_ladder"].items(), StatisticType.RANKED_LADDER)
        data_formatter(statistics["ranked_playoffs"].items(), StatisticType.RANKED_PLAYOFF)

        for name, data in statistics["leagues"].items():
            data_formatter(data.items(), StatisticType.LEAGUE, name)

        for name, data in statistics["competitions"].items():
            data_formatter(data.items(), StatisticType.COMPETITION, name)

        coach_stats_db.save_coach_statistic(coach_id, platform_id, saved_stats)

    @staticmethod
    def get_league_name_alias(league_name: str, platform_id: int, alias_list: list = None):
        if league_name == "":
            # Friendly match
            return "Friendly match"
        if alias_list is None:
            alias_db = Alias()
            alias_list = alias_db.get_alias(AliasType.LEAGUE, platform_id)
        for alias in alias_list:
            if any(x in league_name.lower() for x in alias.get("keywords", [])):
                ret = alias.get("name", league_name)
                return ret.strip()
        return league_name.strip()

    @staticmethod
    def get_competition_name_alias(league_name: str, competition_name: str, platform_id: int, alias_list: list = None):
        if competition_name == "" or league_name == "":
            # Friendly match
            return "Friendly match"
        if alias_list is None:
            alias_db = Alias()
            alias_list = alias_db.get_alias(AliasType.COMPETITION, platform_id)
        for alias in alias_list:
            if alias.get("league_name") == league_name and "_ALL_" in alias.get("keywords", []):
                return alias.get("name", competition_name)
            if any(x in competition_name.lower() and alias.get("league_name") == league_name for x in alias.get("keywords", [])):
                ret = alias.get("name", competition_name)
                return ret.strip()
        return competition_name.strip()

    @staticmethod
    def get_contest_odds(contest: Contest, platform_id: int = 1, force: bool = False):
        matchup_db = Matchup()
        contest_db = Contests()

        if contest.get_coach_home().get_id() is None or contest.get_coach_away().get_id() is None:
            return None

        if not force:
            odds = contest_db.get_odds(contest.get_contest_id(), platform_id)
        else:
            odds = None

        if odds is None:
            home_wr = None
            if contest.get_coach_home().get_id():
                home_wr = StatisticsAPI.get_win_rate(contest.get_coach_home().get_id(), platform_id, race_id=contest.get_team_home().get_race_id())
            if home_wr is not None:
                home_wr = home_wr["win_rate"]
            else:
                home_wr = 51

            away_wr = None
            if contest.get_coach_away().get_id():
                away_wr = StatisticsAPI.get_win_rate(contest.get_coach_away().get_id(), platform_id, race_id=contest.get_team_away().get_race_id())
            if away_wr is not None:
                away_wr = away_wr["win_rate"]
            else:
                away_wr = 49

            coefficient = matchup_db.get_vs_coefficient(contest.get_team_home().get_race(), contest.get_team_away().get_race())
            if coefficient is not None:
                coefficient *= 100
            else:
                coefficient = 50

            diff_wr = home_wr - away_wr
            diff_wr *= 0.4
            home_wr = 50 + diff_wr / 2.
            away_wr = 50 - diff_wr / 2.

            coefficient -= 50
            coefficient *= 0.6
            home_wr += coefficient
            away_wr -= coefficient

            home_odd = 1 / home_wr * 100
            away_odd = 1 / away_wr * 100
            draw_odd = abs(home_odd + away_odd) / 2.

            home_win = round(home_odd, 2)
            draw = round(draw_odd, 2)
            away_win = round(away_odd, 2)
            contest_db.set_odds(contest.get_contest_id(), platform_id, {"home_win": home_win, "draw": draw, "away_win": away_win})
        else:
            home_win = odds["home_win"]
            draw = odds["draw"]
            away_win = odds["away_win"]

        return home_win, draw, away_win
