#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from spike_database.Coaches import Coaches
from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Competition import Competition
from spike_model.Contest import Contest
from spike_requester.CyanideApi import CyanideApi
from spike_utilities.Statistics import Statistics

spike_logger = logging.getLogger("spike_logger")


class LeagueUtilities:

    def __init__(self, client):
        self.__client = client

    @staticmethod
    async def update_league(league_id: int, platform_id: int, update_contest: bool = True):
        rsrc_db = ResourcesRequest()
        league_db = Leagues()
        competition_db = Competitions()
        coach_db = Coaches()
        contest_db = Contests()
        league_data = league_db.get_league_data(league_id, platform_id)
        if league_data is not None:
            platform_id = league_data["platform_id"]
            league_name = league_data["name"]
            league_id = league_data["id"]
            league_logo = league_data["logo"]
            platform = rsrc_db.get_platform_label(platform_id)
            spike_logger.info("League: {} {}".format(league_name, platform))
            competitions = CyanideApi.get_competitions(league_name, 500, platform)

            for competition in competitions.get("competitions", []):
                if competition is not None:
                    competition = Competition(competition)
                    comp_emoji = rsrc_db.get_team_emoji(competition.get_league().get_logo())
                    competition_db.add_or_update_raw_competition(competition, platform_id)

                    competition_db.set_status(competition.get_id(), platform_id, competition.get_status())
                    if competition.get_format() != "ladder" and update_contest is True:
                        limit = int(competition.get_rounds_count()) * int(competition.get_team_max())
                        spike_logger.info("  - Competition: {} {}".format(competition.get_name(), competition.get_id()))

                        if competition.get_status() == 1:
                            spike_logger.info("    - Save scheduled contest")
                            contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(), platform=platform, limit=limit)
                            if contests is None or len(contests.get("upcoming_matches", [])) == 0:
                                contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(), platform=platform, limit=limit, exact=0)

                            for match in contests.get("upcoming_matches", []):
                                match = Contest(match)
                                coach_db.add_or_update_coach(match.get_coach_home().get_id(), match.get_coach_home().get_name(), platform_id)
                                coach_db.add_or_update_coach(match.get_coach_away().get_id(), match.get_coach_away().get_name(), platform_id)
                                contest_db.add_or_update_contest(match, platform_id, competition.get_rounds_count(), comp_emoji)
                                Statistics.get_contest_odds(match, platform_id, True)
                        else:
                            spike_logger.info("    - Ignore schedule :: status: {}".format(competition.get_status()))

                        contest_collected = competition_db.is_contest_fully_collected(competition.get_id(), platform_id)
                        if competition.get_status() > 0 and not contest_collected:
                            spike_logger.info("    - Save played contest")
                            contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(), platform=platform, status="played", limit=limit)
                            if contests is None or len(contests.get("upcoming_matches", [])) == 0:
                                contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(), platform=platform, status="played", limit=limit, exact=0)

                            if contests is not None:
                                for match in contests.get("upcoming_matches", []):
                                    if match is not None:
                                        match = Contest(match)
                                        contest_db.add_or_update_contest(match, platform_id)
                                if competition.get_status() == 2:
                                    spike_logger.info("    - Contest fully collected: {} {}".format(competition.get_name(), competition.get_id()))
                                    competition_db.set_contest_fully_collected(competition.get_id(), platform_id)
                        else:
                            spike_logger.info("    - Ignore played :: status: {} :: collected: {}".format(competition.get_status(), contest_collected))
                    else:
                        spike_logger.info("  -Ignoring :: format: {} name: {} :: id: {}".format(competition.get_format(), competition.get_name(), competition.get_id()))
            league_db.add_or_update_league(league_name, league_id, platform_id, league_logo)
            league_db.update_league_last_update(league_id=league_id, platform_id=platform_id)
