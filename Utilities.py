#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hashlib
import json
import logging
from datetime import datetime
from os import listdir
from typing import List, Dict

import pytz

from spike_database.Coaches import Coaches
from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_database.Leagues import Leagues
from spike_database.MatchHistory import MatchHistory
from spike_database.Matches import Matches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Match import Match
from spike_translation import Translator

spike_logger = logging.getLogger('spike_logger')


class Utilities:

    @staticmethod
    def hash_string(data: str):
        hash_object = hashlib.sha512(data.encode())
        return hash_object.hexdigest()

    @staticmethod
    def write_dictionary_to_json_file(data: Dict, file_name: str):
        with open('json/' + file_name, 'w', encoding='utf-8') as fp:
            json.dump(data, fp)

    @staticmethod
    def write_json_matches_into_match_history(matches: List[Dict]):
        rsrc_db = ResourcesRequest()
        match_history_db = MatchHistory()
        match_db = Matches()
        coach_db = Coaches()
        league_db = Leagues()
        competition_db = Competitions()
        coach_to_update = []

        try:
            match_ids = []

            for match in matches:
                uuid = match['uuid']
                spike_logger.info('Save match: {}'.format(uuid))
                match_ids.append(uuid)
                match = Match(match)
                platform_id = rsrc_db.get_platform_id(match.get_platform())

                fmt = '%Y-%m-%d %H:%M:%S'
                d1 = datetime.strptime(match.get_start_datetime(), fmt)
                d2 = datetime.strptime(match.get_finish_datetime(), fmt)

                duration = (d2 - d1).seconds
                if duration > 0:
                    duration = int(duration / 60)

                if match.get_team_home().get_nb_mvp() == match.get_team_away().get_nb_mvp() == 1:
                    concede_home = 0
                    concede_away = 0
                elif match.get_team_away().get_nb_mvp() == 2:
                    concede_home = 0
                    concede_away = 1
                else:
                    concede_home = 1
                    concede_away = 0

                match_data = {
                    '_id': uuid,
                    'competition_name': match.get_competition_name(),
                    'competition_id': match.get_competition_id(),
                    'league_name': match.get_league_name(),
                    'league_id': match.get_league_id(),
                    'platform': int(platform_id),
                    'platform_id': int(platform_id),
                    'date': match.get_start_datetime()[:10],
                    'duration': duration,
                    'version': 2,
                    'team_home': {
                        'name': match.get_team_home().get_name(),
                        'tv': match.get_team_home().get_team_value(),
                        'coach_id': match.get_coach_home().get_id(),
                        'conceded': concede_home,
                        'race_id': match.get_team_home().get_race_id(),
                        'logo': match.get_team_home().get_logo(),
                        'score': match.get_score_home()
                    },
                    'team_away': {
                        'name': match.get_team_away().get_name(),
                        'tv': match.get_team_away().get_team_value(),
                        'coach_id': match.get_coach_away().get_id(),
                        'concede': concede_away,
                        'race_id': match.get_team_away().get_race_id(),
                        'logo': match.get_team_away().get_logo(),
                        'score': match.get_score_away()
                    },
                }

                match_history_db.add_or_update_match(str(uuid), match_data)
                league_db.add_or_update_league(match.get_league_name(), match.get_league_id(), platform_id)
                competition_db.add_or_update_competition(match.get_competition_name(), match.get_competition_id(), match.get_league_id(), platform_id)
                coach_db.add_or_update_coach(match.get_coach_home().get_id(), match.get_coach_home().get_name(), platform_id)
                coach_db.add_or_update_coach(match.get_coach_away().get_id(), match.get_coach_away().get_name(), platform_id)
                coach_to_update.append((match.get_coach_home().get_id(), platform_id))
                coach_to_update.append((match.get_coach_away().get_id(), platform_id))

            match_db.set_match_saved_in_match_history(match_ids)
        except Exception as e:
            spike_logger.error(f"Error {e}")

        return coach_to_update

    @staticmethod
    def get_platform_in_args(args: List[str]):
        common_db = ResourcesRequest()
        platform = None
        for arg in args:
            if common_db.get_platform_id(arg) is not None:
                platform = arg
        return platform

    @staticmethod
    def get_discord_id(label: str):
        return int(label.replace('#', "").replace('<', "").replace('>', "").replace('@', "").replace('&', "").replace('!', ""))

    @staticmethod
    def get_logo_id(discord_emoji: str):
        logo = discord_emoji.split(':')[2]
        return Utilities.get_discord_id(logo)

    @staticmethod
    def get_excluded_server():
        exclude_server = [469824070829998080, 469826994461343744, 469824117735030784, 469822191857434634,
                          469822722080505856, 469823592125825034, 469823897185943576, 469823666612600832,
                          469823738196787210, 469823770924941312, 469823810116386817, 469823861664514079,
                          469823628628983848, 469824479657459725, 469824249641566210, 474525952345505793,
                          264445053596991498]
        return exclude_server

    @staticmethod
    def convert_camel_case_to_readable(label: str):
        return ''.join(map(lambda x: x if x.islower() else ' ' + x, label))

    @staticmethod
    def get_contest(competition_id: int, platform_id: int, status: str, standing_source: str = None):
        competition_db = Competitions()
        contest_db = Contests()
        contest_list = []
        status_dict = {
            'all': None,
            'scheduled': 1,
            'played': 2
        }

        competition_data = competition_db.get_competition_data(competition_id, platform_id)
        if standing_source is None:
            standing_source = competition_data.get('standing_source')
            if standing_source is None and len(contest_db.get_contests_in_competition(competition_id, platform_id)) > 0:
                standing_source = 'spike_standing'

        if competition_data.get('format') != 'ladder':
            if competition_data.get('format') == 'single_elimination':
                contest_list = contest_db.get_contests_in_competition(competition_id, platform_id, status_dict[status])
                team_max = competition_data.get('teams_max', 0)
                if len(contest_list) == (team_max - 1):
                    return contest_list

            if standing_source in ['spike_standing', 'spikoween'] or competition_data.get('contest_fully_collected', False):
                contest_list = contest_db.get_contests_in_competition(competition_id, platform_id, status_dict[status])
        return contest_list

    @staticmethod
    def get_guilds_id():
        guilds_id = []
        directory = 'databases/'

        for db_file in listdir(directory):
            try:
                if db_file.endswith('.db') and db_file not in ['Rules.db', 'privateChannel.db', 'common.db', '0.db']:
                    db_file = db_file.replace('.db', "")
                    guilds_id.append(int(db_file))
            except:
                pass
        return guilds_id

    @staticmethod
    def get_guilds_id_str():
        guilds_id = []
        directory = 'databases/'

        for db_file in listdir(directory):
            try:
                if db_file.endswith('.db') and db_file not in ['Rules.db', 'privateChannel.db', 'common.db', '0.db']:
                    db_file = db_file.replace('.db', "")
                    guilds_id.append(db_file)
            except:
                pass
        return guilds_id

    @staticmethod
    def is_cabal_vision(league_id: int, platform_id: int = 1):
        if (league_id == 1 and (platform_id == 1 or platform_id)) or (league_id == 3 and platform_id == 3):
            return True
        return False

    @staticmethod
    def get_timezones():
        timezones = []
        for timezone in pytz.all_timezones:
            timezones.append((timezone, timezone))
        return timezones

    @staticmethod
    def get_lang():
        return [
            ('AF', 'Afrikanns'), ('SQ', 'Albanian'), ('AR', 'Arabic'), ('HY', 'Armenian'), ('EU', 'Basque'), ('BN', 'Bengali'), ('BG', 'Bulgarian'),
            ('CA', 'Catalan'), ('KM', 'Cambodian'),
            ('ZH', 'Chinese (Mandarin)'), ('HR', 'Croation'), ('CS', 'Czech'), ('DA', 'Danish'), ('NL', 'Dutch'), ('EN', 'English'),
            ('ET', 'Estonian'), ('FJ', 'Fiji'), ('FI', 'Finnish'),
            ('FR', 'French'), ('KA', 'Georgian'), ('DE', 'German'), ('EL', 'Greek'), ('GU', 'Gujarati'), ('HE', 'Hebrew'), ('HI', 'Hindi'),
            ('HU', 'Hungarian'), ('IS', 'Icelandic'),
            ('ID', 'Indonesian'), ('GA', 'Irish'), ('IT', 'Italian'), ('JA', 'Japanese'), ('JW', 'Javanese'), ('KO', 'Korean'), ('LA', 'Latin'),
            ('LV', 'Latvian'), ('LT', 'Lithuanian'),
            ('MK', 'Macedonian'), ('MS', 'Malay'), ('ML', 'Malayalam'), ('MT', 'Maltese'), ('MI', 'Maori'), ('MR', 'Marathi'), ('MN', 'Mongolian'),
            ('NE', 'Nepali'), ('NO', 'Norwegian'),
            ('FA', 'Persian'), ('PL', 'Polish'), ('PT', 'Portuguese'), ('PA', 'Punjabi'), ('QU', 'Quechua'), ('RO', 'Romanian'), ('RU', 'Russian'),
            ('SM', 'Samoan'), ('SR', 'Serbian'),
            ('SK', 'Slovak'), ('SL', 'Slovenian'), ('ES', 'Spanish'), ('SW', 'Swahili'), ('SE', 'Swedish '), ('TA', 'Tamil'), ('TT', 'Tatar'),
            ('TE', 'Telugu'), ('TH', 'Thai'), ('BO', 'Tibetan'),
            ('TO', 'Tonga'), ('TR', 'Turkish'), ('UK', 'Ukranian'), ('UR', 'Urdu'), ('UZ', 'Uzbek'), ('VI', 'Vietnamese'), ('WLS', 'Welsh'),
            ('XH', 'Xhosa')]

    @staticmethod
    def get_country():
        return [
            ('AF', 'Afghanistan'), ('AX', 'Åland Islands'), ('AL', 'Albania'), ('DZ', 'Algeria'), ('AS', 'American Samoa'), ('AD', 'Andorra'),
            ('AO', 'Angola'), ('AI', 'Anguilla'),
            ('AQ', 'Antarctica'), ('AG', 'Antigua and Barbuda'), ('AR', 'Argentina'), ('AM', 'Armenia'), ('AW', 'Aruba'), ('AU', 'Australia'),
            ('AT', 'Austria'), ('AZ', 'Azerbaijan'),
            ('BS', 'Bahamas'), ('BH', 'Bahrain'), ('BD', 'Bangladesh'), ('BB', 'Barbados'), ('BY', 'Belarus'), ('BE', 'Belgium'), ('BZ', 'Belize'),
            ('BJ', 'Benin'), ('BM', 'Bermuda'),
            ('BT', 'Bhutan'), ('BO', 'Bolivia, Plurinational State of'), ('BQ', 'Bonaire, Sint Eustatius and Saba'), ('BA', 'Bosnia and Herzegovina'),
            ('BW', 'Botswana'), ('BV', 'Bouvet Island'),
            ('BR', 'Brazil'), ('IO', 'British Indian Ocean Territory'), ('BN', 'Brunei Darussalam'), ('BG', 'Bulgaria'), ('BF', 'Burkina Faso'),
            ('BI', 'Burundi'), ('KH', 'Cambodia'),
            ('CM', 'Cameroon'), ('CA', 'Canada'), ('CV', 'Cape Verde'), ('KY', 'Cayman Islands'), ('CF', 'Central African Republic'), ('TD', 'Chad'),
            ('CL', 'Chile'), ('CN', 'China'),
            ('CX', 'Christmas Island'), ('CC', 'Cocos (Keeling) Islands'), ('CO', 'Colombia'), ('KM', 'Comoros'), ('CG', 'Congo'),
            ('CD', 'Congo, the Democratic Republic of the'),
            ('CK', 'Cook Islands'), ('CR', 'Costa Rica'), ('CI', 'Côte d\'Ivoire'), ('HR', 'Croatia'), ('CU', 'Cuba'), ('CW', 'Curaçao'), ('CY',
                                                                                                                                           'Cyprus'),
            ('CZ', 'Czech Republic'), ('DK', 'Denmark'),
            ('DJ', 'Djibouti'), ('DM', 'Dominica'), ('DO', 'Dominican Republic'), ('EC', 'Ecuador'), ('EG', 'Egypt'), ('SV', 'El Salvador'),
            ('GQ', 'Equatorial Guinea'), ('ER', 'Eritrea'),
            ('EE', 'Estonia'), ('ET', 'Ethiopia'), ('FK', 'Falkland Islands (Malvinas)'), ('FO', 'Faroe Islands'), ('FJ', 'Fiji'), ('FI', 'Finland'),
            ('FR', 'France'), ('GF', 'French Guiana'),
            ('PF', 'French Polynesia'), ('TF', 'French Southern Territories'), ('GA', 'Gabon'), ('GM', 'Gambia'), ('GE', 'Georgia'),
            ('DE', 'Germany'), ('GH', 'Ghana'), ('GI', 'Gibraltar'),
            ('GR', 'Greece'), ('GL', 'Greenland'), ('GD', 'Grenada'), ('GP', 'Guadeloupe'), ('GU', 'Guam'), ('GT', 'Guatemala'), ('GG', 'Guernsey'),
            ('GN', 'Guinea'), ('GW', 'Guinea-Bissau'),
            ('GY', 'Guyana'), ('HT', 'Haiti'), ('HM', 'Heard Island and McDonald Islands'), ('VA', 'Holy See (Vatican City State)'),
            ('HN', 'Honduras'), ('HK', 'Hong Kong'), ('HU', 'Hungary'),
            ('IS', 'Iceland'), ('IN', 'India'), ('ID', 'Indonesia'), ('IR', 'Iran, Islamic Republic of'), ('IQ', 'Iraq'), ('IE', 'Ireland'),
            ('IM', 'Isle of Man'), ('IL', 'Israel'), ('IT', 'Italy'),
            ('JM', 'Jamaica'), ('JP', 'Japan'), ('JE', 'Jersey'), ('JO', 'Jordan'), ('KZ', 'Kazakhstan'), ('KE', 'Kenya'), ('KI', 'Kiribati'),
            ('KP', 'Korea, Democratic People\'s Republic of'),
            ('KR', 'Korea, Republic of'), ('KW', 'Kuwait'), ('KG', 'Kyrgyzstan'), ('LA', 'Lao People\'s Democratic Republic'), ('LV', 'Latvia'),
            ('LB', 'Lebanon'), ('LS', 'Lesotho'), ('LR', 'Liberia'),
            ('LY', 'Libya'), ('LI', 'Liechtenstein'), ('LT', 'Lithuania'), ('LU', 'Luxembourg'), ('MO', 'Macao'),
            ('MK', 'Macedonia, the former Yugoslav Republic of'), ('MG', 'Madagascar'),
            ('MW', 'Malawi'), ('MY', 'Malaysia'), ('MV', 'Maldives'), ('ML', 'Mali'), ('MT', 'Malta'), ('MH', 'Marshall Islands'),
            ('MQ', 'Martinique'), ('MR', 'Mauritania'), ('MU', 'Mauritius'),
            ('YT', 'Mayotte'), ('MX', 'Mexico'), ('FM', 'Micronesia, Federated States of'), ('MD', 'Moldova, Republic of'), ('MC', 'Monaco'),
            ('MN', 'Mongolia'), ('ME', 'Montenegro'),
            ('MS', 'Montserrat'), ('MA', 'Morocco'), ('MZ', 'Mozambique'), ('MM', 'Myanmar'), ('NA', 'Namibia'), ('NR', 'Nauru'), ('NP', 'Nepal'),
            ('NL', 'Netherlands'), ('NC', 'New Caledonia'),
            ('NZ', 'New Zealand'), ('NI', 'Nicaragua'), ('NE', 'Niger'), ('NG', 'Nigeria'), ('NU', 'Niue'), ('NF', 'Norfolk Island'),
            ('MP', 'Northern Mariana Islands'), ('NO', 'Norway'),
            ('OM', 'Oman'), ('PK', 'Pakistan'), ('PW', 'Palau'), ('PS', 'Palestinian Territory, Occupied'), ('PA', 'Panama'),
            ('PG', 'Papua New Guinea'), ('PY', 'Paraguay'), ('PE', 'Peru'),
            ('PH', 'Philippines'), ('PN', 'Pitcairn'), ('PL', 'Poland'), ('PT', 'Portugal'), ('PR', 'Puerto Rico'), ('QA', 'Qatar'),
            ('RE', 'Réunion'), ('RO', 'Romania'), ('RU', 'Russian Federation'),
            ('RW', 'Rwanda'), ('BL', 'Saint Barthélemy'), ('SH', 'Saint Helena, Ascension and Tristan da Cunha'), ('KN', 'Saint Kitts and Nevis'),
            ('LC', 'Saint Lucia'),
            ('MF', 'Saint Martin (French part)'), ('PM', 'Saint Pierre and Miquelon'), ('VC', 'Saint Vincent and the Grenadines'), ('WS', 'Samoa'),
            ('SM', 'San Marino'),
            ('ST', 'Sao Tome and Principe'), ('SA', 'Saudi Arabia'), ('SN', 'Senegal'), ('RS', 'Serbia'), ('SC', 'Seychelles'),
            ('SL', 'Sierra Leone'), ('SG', 'Singapore'),
            ('SX', 'Sint Maarten (Dutch part)'), ('SK', 'Slovakia'), ('SI', 'Slovenia'), ('SB', 'Solomon Islands'), ('SO', 'Somalia'),
            ('ZA', 'South Africa'), ('SCT', 'Scotland'),
            ('GS', 'South Georgia and the South Sandwich Islands'), ('SS', 'South Sudan'), ('ES', 'Spain'), ('LK', 'Sri Lanka'), ('SD', 'Sudan'),
            ('SR', 'Suriname'), ('SJ', 'Svalbard and Jan Mayen'),
            ('SZ', 'Swaziland'), ('SE', 'Sweden'), ('CH', 'Switzerland'), ('SY', 'Syrian Arab Republic'), ('TW', 'Taiwan, Province of China'),
            ('TJ', 'Tajikistan'),
            ('TZ', 'Tanzania, United Republic of'), ('TH', 'Thailand'), ('TL', 'Timor-Leste'), ('TG', 'Togo'), ('TK', 'Tokelau'), ('TO', 'Tonga'),
            ('TT', 'Trinidad and Tobago'), ('TN', 'Tunisia'),
            ('TR', 'Turkey'), ('TM', 'Turkmenistan'), ('TC', 'Turks and Caicos Islands'), ('TV', 'Tuvalu'), ('UG', 'Uganda'), ('UA', 'Ukraine'),
            ('AE', 'United Arab Emirates'), ('NIR', 'North Irland'),
            ('GB', 'United Kingdom'), ('US', 'United States'), ('UM', 'United States Minor Outlying Islands'), ('UY', 'Uruguay'),
            ('UZ', 'Uzbekistan'), ('VU', 'Vanuatu'), ('ENG', 'England'),
            ('VE', 'Venezuela, Bolivarian Republic of'), ('VN', 'Viet Nam'), ('VG', 'Virgin Islands, British'), ('VI', 'Virgin Islands, U.S.'),
            ('WF', 'Wallis and Futuna'), ('WLS', 'Wales'),
            ('EH', 'Western Sahara'), ('YE', 'Yemen'), ('ZM', 'Zambia'), ('ZW', 'Zimbabwe')
        ]

    @staticmethod
    def get_round_label(competition_format, current_round, round_count, lang):
        if competition_format == 'single_elimination':
            if current_round == round_count:
                # /* Final */
                round_display = Translator.tr('#_schedule_command.final', lang)
            elif current_round == (round_count - 1):
                # /* Semi final */
                round_display = Translator.tr('#_schedule_command.semi_final', lang)
            elif current_round == (round_count - 2):
                # /* Quarter final */
                round_display = Translator.tr('#_schedule_command.quarter_final', lang)
            elif current_round == (round_count - 3):
                # /* Last 16 */
                round_display = Translator.tr('#_schedule_command.last_16', lang)
            elif current_round == (round_count - 4):
                # /* Last 32 */
                round_display = Translator.tr('#_schedule_command.last_32', lang)
            elif current_round == (round_count - 5):
                # /* Last 64 */
                round_display = Translator.tr('#_schedule_command.last_64', lang)
            else:
                # /* Round */
                round_label = Translator.tr('#_schedule_command.round_label', lang)
                round_display = Translator.tr(f"{round_label} {current_round}/{round_count}")
        else:
            # /* Round */
            round_label = Translator.tr('#_schedule_command.round_label', lang)
            round_display = Translator.tr(f"{round_label} {current_round}/{round_count}")
        return round_display
